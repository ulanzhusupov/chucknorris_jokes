/* 
// Example Usage
Map<String, dynamic> map = jsonDecode(<myJSONString>);
var myChuckNorrisModelNode = ChuckNorrisModel.fromJson(map);
*/
class ChuckNorrisModel {
  List<dynamic>? categories;
  String? createdat;
  String? iconurl;
  String? id;
  String? updatedat;
  String? url;
  String? value;
  String? translated;

  ChuckNorrisModel(
      {this.categories,
      this.createdat,
      this.iconurl,
      this.id,
      this.updatedat,
      this.url,
      this.value});

  ChuckNorrisModel.fromJson(Map<String, dynamic> json) {
    if (json['categories'] != null) {
      categories = <dynamic>[];
      json['categories'].forEach((v) {
        categories!.add(v.fromJson(v));
      });
    }
    createdat = json['created_at'];
    iconurl = json['icon_url'];
    id = json['id'];
    updatedat = json['updated_at'];
    url = json['url'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['categories'] = categories != null
        ? categories!.map((v) => v?.toJson()).toList()
        : null;
    data['created_at'] = createdat;
    data['icon_url'] = iconurl;
    data['id'] = id;
    data['updated_at'] = updatedat;
    data['url'] = url;
    data['value'] = value;
    return data;
  }
}
