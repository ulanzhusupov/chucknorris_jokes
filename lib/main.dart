import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_chucknorris_api/ChuckNorrisModel.dart';
import 'package:flutter_chucknorris_api/favorite_screen.dart';
import 'package:translator/translator.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const RandomJokeScreen(),
    );
  }
}

class RandomJokeScreen extends StatefulWidget {
  const RandomJokeScreen({super.key});

  @override
  State<RandomJokeScreen> createState() => _RandomJokeScreenState();
}

class _RandomJokeScreenState extends State<RandomJokeScreen> {
  List<Map> favorites = [];
  final String original = "";
  final String translated = "";
  String lang = "en";
  ChuckNorrisModel? chuckModel;
  bool wantTranslate = false;

  @override
  void initState() {
    super.initState();
    getRandomChuckNorrisJoke();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const SizedBox(height: 50),
              InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            FavoriteScreens(favorites: favorites)),
                  );
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Icon(
                      Icons.bookmark_add,
                      color: Colors.deepPurple,
                      size: 30,
                    ),
                    Text(
                      favorites.length.toString(),
                      style: const TextStyle(
                          fontSize: 22,
                          fontWeight: FontWeight.w500,
                          color: Colors.deepPurple),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 20),
              Text(
                wantTranslate
                    ? chuckModel?.translated ?? ""
                    : chuckModel?.value ?? "",
                textAlign: TextAlign.center,
                style: const TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                    color: Colors.black),
              ),
              const Spacer(),
              SizedBox(
                width: 256,
                height: 56,
                child: ElevatedButton(
                  onPressed: () {
                    Map<String, String> joke = {
                      "original": chuckModel?.value ?? "empty",
                      "translated": chuckModel?.translated ?? "empty"
                    };
                    favorites.add(joke);
                    setState(() {});
                  },
                  child: const Icon(
                    Icons.favorite,
                    color: Colors.red,
                  ),
                ),
              ),
              const SizedBox(height: 10),
              SizedBox(
                width: 256,
                height: 56,
                child: ElevatedButton(
                  onPressed: () {
                    wantTranslate = !wantTranslate;
                    setState(() {});
                  },
                  child: const Text(
                    "Перевести",
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 10),
              SizedBox(
                width: 256,
                height: 56,
                child: ElevatedButton(
                  onPressed: getRandomChuckNorrisJoke,
                  child: const Icon(Icons.refresh),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> getRandomChuckNorrisJoke() async {
    final Dio dio = Dio();
    final Response response =
        await dio.get("https://api.chucknorris.io/jokes/random");

    chuckModel = ChuckNorrisModel.fromJson(response.data);
    chuckModel?.translated = await translateTo();
    setState(() {});
  }

  Future<String> translateTo() async {
    final translator = GoogleTranslator();

    var translation = await translator.translate(chuckModel?.value ?? "",
        from: lang, to: lang == 'en' ? 'ru' : 'en');

    return translation.toString();
  }
}
